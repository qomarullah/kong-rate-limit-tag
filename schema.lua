local typedefs = require "kong.db.schema.typedefs"


local ORDERED_PERIODS = { "second", "minute", "hour", "day", "month", "year"}


local function validate_periods_order(config)
  for i, lower_period in ipairs(ORDERED_PERIODS) do
    local v1 = config[lower_period]
    if type(v1) == "number" then
      for j = i + 1, #ORDERED_PERIODS do
        local upper_period = ORDERED_PERIODS[j]
        local v2 = config[upper_period]
        if type(v2) == "number" and v2 < v1 then
          return nil, string.format("The limit for %s(%.1f) cannot be lower than the limit for %s(%.1f)",
                                    upper_period, v2, lower_period, v1)
        end
      end
    end
  end

  return true
end


local function is_dbless()
  local _, database, role = pcall(function()
    return kong.configuration.database,
           kong.configuration.role
  end)

  return database == "off" or role == "control_plane"
end


local policy
if is_dbless() then
  policy = {
    type = "string",
    default = "local",
    len_min = 0,
    one_of = {
      "local",
      "redis",
    },
  }

else
  policy = {
    type = "string",
    default = "cluster",
    len_min = 0,
    one_of = {
      "local",
      "cluster",
      "redis",
    },
  }
end


return {
  name = "rate-limiting-tag",
  fields = {
    { protocols = typedefs.protocols_http },
    { config = {
        type = "record",
        fields = {
          { tag_only = { type = "boolean", default=false } },
          { tag_value_add = { type = "string", default="api-1", required = true } },
          { trx_per_second = { type = "number" }, },
          { trx_per_minute = { type = "number"}, },
          --{ interval_check = { type = "number", gt = 0, required = true, default = 10 }},
          { tag_header = {type = "string", default = "x-last-access" }},
          { tag_allow = { type = "array",required = true, default = {"api-1",}, elements = { type = "string" }, }, },
          { tag_ttl = { type = "integer", required = true, default=600,gt = 0 } },
          { tag_rate_limit_countdown = {type = "integer", required = true, default=30,gt = 0 } },
          --{ tag_key_encrypt = {type = "string", required = true, default="Tsel#2023!" } },
          --{ status_code = { type = "number", required = true, default=429 } },
	        --{ response_body = { type = "string", default ="Rate Limit Reach" } },
         { status_code = { type = "number", required = true, default=429 } },
         { response_body = { type = "string", default = "{\"status\":\"00000\",\"message\":\"Success\",\"data\":{\"page\":\"home\",\"maintenance_mode\":true,\"countdownretry\":30,\"periodstart\":\"2021-30-06T00:00:00+07:00\",\"periodend\":\"2021-30-06T05:00:00+07:00\",\"title\":\"Mohon Menunggu\",\"desc\":\"Untuk menghindari ketidaknyamanan, saat ini layanan sedang ditutup terlebih dahulu. Silakan akses kembali MyTelkomsel setelah 1 menit\",\"navigationEnable\":true,\"maintenanceInfo\":[]},\"transaction_id\":\"A3P12210111541562180\"}" } },
        
        },
        --custom_validator = validate_periods_order,
      },
    },
  },
  --entity_checks = {
   -- { at_least_one_of = { "config.second", "config.minute"} },
  --},
}
