return {
  postgres = {
    up = [[
      CREATE INDEX IF NOT EXISTS ratelimiting_metrics_idx_tag ON ratelimiting_metrics_tag (service_id, route_id, period_date, period);
    ]],
  },

  cassandra = {
    up = [[
    ]],
  },
}
